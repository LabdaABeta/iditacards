\LoadClass{standalone}
% NOTE: Cloud colour is 76baff

\ProvidesClass{iditacard}[2017/05/12 Class for Iditacard cards]

\RequirePackage{fontspec}
\RequirePackage{xcolor}
\RequirePackage{xstring}

\RequirePackage[none]{hyphenat}

\RequirePackage{tikz}
\usetikzlibrary{positioning,shapes,shadows,arrows,backgrounds,fit}
\RequirePackage{pgfplots}
\RequirePackage{shapepar}
\RequirePackage{microtype}
\RequirePackage{mathtools}
\let\mttext\text

\graphicspath{{images/}}

\newfontfamily\bebas{Bebas Neue Regular}
\newfontfamily\alegreya{Alegreya}

\definecolor{stage_one}{HTML}{7FDF7D}
\definecolor{stage_two}{HTML}{9FB76F}
\definecolor{stage_three}{HTML}{5F5FFF}
\definecolor{stage_four}{HTML}{3FDFDF}
\definecolor{stage_five}{HTML}{DFDF3F}
\definecolor{stage_six}{HTML}{9F9F9F}
\definecolor{stage_seven}{HTML}{FF5F5F}
\definecolor{stage_eight}{HTML}{CF5FCF}

\definecolor{town}{HTML}{CF5FCF}
\definecolor{frozenlake}{HTML}{5F9FFF}
\definecolor{hills}{HTML}{7FDF7F}
\definecolor{wasteland}{HTML}{DFDF3F}
\definecolor{forest}{HTML}{9FB76F}
\definecolor{cliffface}{HTML}{FF5F5F}
\definecolor{ropebridge}{HTML}{CFAF7F}
\definecolor{blizzard}{HTML}{5F5FFF}
\definecolor{mountain}{HTML}{9F9F9F}
\definecolor{village}{HTML}{FF8F0F}

\definecolor{energy}{HTML}{003FFF}
\definecolor{health}{HTML}{FF0000}
\definecolor{risk}{HTML}{000000}

\definecolor{utility}{HTML}{FFFFFF}
\definecolor{attachment}{HTML}{5FAFCF}
\definecolor{dog}{HTML}{BF9F7F}
\definecolor{stretch}{HTML}{9F9F9F}
\definecolor{personal}{HTML}{AF5FCF}
\definecolor{movement}{HTML}{7F7FDF}
\definecolor{damage}{HTML}{FF6F6F}
\definecolor{sled}{HTML}{7FDF7F}
\definecolor{food}{HTML}{CFCF4F}

\definecolor{info}{HTML}{FFFFFF}
\definecolor{starting}{HTML}{777777}
\definecolor{common}{HTML}{000000}
\definecolor{rare}{HTML}{000077}
\definecolor{epic}{HTML}{007700}
\definecolor{legendary}{HTML}{770077}

\newcommand{\cardtype}[1]{%
    \newcommand{\backgroundcolor}{#1}
}

\newcommand{\rarity}[1]{%
    \newcommand{\edgecolor}{#1}
}

\newcommand{\deck}[1]{%
    \newcommand{\whichdeck}{#1}
}

\newcommand{\energy}[1]{%
\node [minimum width=0.5in, minimum height=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0,825.0/300.0) {\includegraphics[width=0.5in,height=0.5in]{icons/hand.png}};
\StrLen{#1}[\arglen]
\ifnum\arglen>1%
    \node [text=black, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0,825.0/300.0) {\bf\fontsize{14}{14}\bebas #1};%
\else
    \node [text=black, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0,825.0/300.0) {\bf\fontsize{25}{30}\bebas #1};%
\fi
}

\newcommand{\health}[1]{%
\node [minimum width=0.5in, minimum height=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0,700.0/300.0) {\includegraphics[width=0.5in, height=0.5in]{icons/deck.png}};
\StrLen{#1}[\arglen]
\ifnum\arglen>1%
\node [text=black, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0, 700.0/300.0) {\bf\fontsize{14}{14}\bebas #1};%
\else
\node [text=black, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0, 700.0/300.0) {\bf\fontsize{25}{30}\bebas #1};%
\fi
}

\newcommand{\risk}[1]{%
\node [minimum width=0.5in, minimum height=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0, 538.0/300.0) {\includegraphics[width=0.5in, height=0.5in]{icons/risk.png}};
\StrLen{#1}[\arglen]
\ifnum\arglen>1%
\node [text=white, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0, 538.0/300.0) {\bf\fontsize{14}{14}\bebas #1};%
\else
\node [text=white, minimum width=0.5in, minimum height=0.5in, text centered, text width=0.5in, anchor=center, inner sep=0mm] at (125.0/300.0, 538.0/300.0) {\bf\fontsize{25}{30}\bebas #1};%
\fi
}

\newenvironment{costcounts}
{% Begin
\begin{axis}[at={(300,350)},
    y axis line style={draw=none},
    tick style={draw=none},
    width=1in,
    height=1in,
    yticklabels={,,},
    enlargelimits=0.05,
    ybar interval=1.0]
}
{% End
\end{axis}}

\newcommand{\energycounts}[1]{\addplot[draw=none,fill=energy] coordinates {#1};}
\newcommand{\healthcounts}[1]{\addplot[draw=none,fill=health] coordinates {#1};}
\newcommand{\riskcounts}[1]{\addplot[draw=none,fill=risk] coordinates {#1};}

\newcommand{\@condcost}[2]{\ifnum 0<#1#2\fi}
\newcommand{\costs}[3]{%
\@condcost{#1}{\energy{#1}}%
\@condcost{#2}{\health{#2}}%
\@condcost{#3}{\risk{#3}}%
}

\newcommand{\name}[1]{%
% add draw to the node to see its box
\node [rectangle, anchor=north, minimum width=2.15in, minimum height=2.0/3.0in, text centered, text width=1.833333in, inner sep=0mm] at (375.0/300.0,975.0/300.0) {\bf\fontsize{20}{30}\bebas#1\par};
}

\renewcommand{\text}[1]{%
% add draw to the node to see its box
\node [rectangle, minimum width=1.833333in, minimum height=200.0/300.0, text centered, text width=1.833333in, inner sep=0mm, anchor=north] at (375.0/300.0,450.0/300) {\fontsize{16}{16}\bebas#1\par};
}

\newcommand{\helptext}[1]{%
% add draw to the node to see its box
\node [rectangle, minimum width=1.833333in, minimum height=200.0/300.0, text centered, text width=1.833333in, inner sep=0mm, anchor=north] at (375.0/300.0, 800.0/300.0) {\fontsize{16}{16}\bebas#1\par};
}

\newcommand{\flava}[1]{%
% add draw to the node to see its box
\node [rectangle, minimum width=1.833333in, minimum height=100.0/300.0, text centered, text width=1.833333in, inner sep=0mm, anchor=north] at (375.0/300.0,150.0/300.0) {\fontsize{6}{6}\alegreya"#1"\par};
}

\newcommand{\type}[1]{%
\node [rectangle, minimum width=1.833333in, minimum height=100.0/300.0, text centered, text width=1.833333in, inner sep=0mm, anchor=north] at (375.0/300.0,100.0/300.0) {\fontsize{12}{12}\bebas#1\par};
}

% Aspect ratio of about 3:2
\newcommand{\art}[1]{%
\node [rectangle, minimum width=649.8/300.0, minimum height=425.0/300.0, inner sep=0, anchor=north west] at (49.2/300.0, 888.0/300.0) {\noindent\includegraphics*[width=2.1664in, height=1.416667in]{#1}};
}

\newcommand{\utilcount}[1]{%
\node [anchor=center] at (274.2/300.0, 677.0/300.0) {#1}; % Coord is art NW +- offset of image
}

\newcommand{\dogcount}[1]{%
\node [anchor=center] at (364.2/300.0, 807.0/300.0) {#1};
}

\newcommand{\movecount}[1]{%
\node [anchor=center] at (634.2/300.0, 677.0/300.0) {#1};
}

\newcommand{\foodcount}[1]{%
\node [anchor=center] at (544.2/300.0, 807.0/300.0) {#1};
}

\newcommand{\attachcount}[1]{%
\node [anchor=center] at (364.2/300.0, 547.0/300.0) {#1};
}

\newcommand{\personalcount}[1]{%
\node [anchor=center] at (454.2/300.0, 677.0/300.0) {#1};
}

\newcommand{\sledcount}[1]{%
\node [anchor=center] at (544.2/300.0, 547.0/300.0) {#1};
}

\newcommand{\energycount}[1]{%
\energy{#1}
%\node [anchor=center] at (0,0) {#1};
}

\newcommand{\healthcount}[1]{%
\health{#1}
%\node [anchor=center] at (1.0,1.0) {#1};
}

\newcommand{\riskcount}[1]{%
\risk{#1}
%\node [anchor=center] at (2.0,2.0) {#1};
}

\newcommand{\useicon}[1]{\raisebox{-0.1em}{\includegraphics[height=1em]{icons/#1.png}}}
\newcommand{\upto}[1]{$\xrightarrow{\mttext{up to #1}}$}
\newcommand{\sendto}[1]{$\xrightarrow{\mttext{#1}}$}

\newcommand{\Mm}{Mm}
\newcommand{\Mms}{Mms}
\newcommand{\move}[1]{\raisebox{-0.4em}{\includegraphics[height=1.5em]{icons/dog-sled-icon.png}} #1}
\newcommand{\destroy}[2]{\useicon{eye} #2 \includegraphics[height=1em]{icons/trash-can.png} #1} % Destroy up to #1 of #2
\newcommand{\take}[2]{\useicon{backpack} #2 \useicon{shopping-cart} #1}
\newcommand{\draw}[1]{Draw \ifnum 1=#1 a \else #1 \fi card\ifnum 1<#1s\fi}
\newcommand{\daydraw}[1]{Draw \ifnum 1<#1 #1 extra cards \else an extra card \fi when you take a new day.}
\newcommand{\personal}{\colorbox{personal}{personal}}
\newcommand{\attachment}{\colorbox{attachment}{attachment}}
\newcommand{\sled}{\colorbox{sled}{sled}}
\newcommand{\hypothermia}{\useicon{hypo}}
\newcommand{\starvation}{\useicon{starve}}
\newcommand{\speed}[1]{\useicon{speed}#1}
\newcommand{\healthcosts}[1]{\begin{tikzpicture}[x=1in,y=1in]\node [centered, inner sep=0pt] at (0,0) {\includegraphics[width=0.5in,height=0.5in]{icons/deck.png}}; \node [centered, inner sep=0pt] at (0,0) {#1}; \end{tikzpicture}}
\newcommand{\energycosts}[1]{\begin{tikzpicture}[x=1in,y=1in]\node [centered, inner sep=0pt] at (0,0) {\includegraphics[width=0.5in,height=0.5in]{icons/hand.png}}; \node [centered, inner sep=0pt] at (0,0) {#1}; \end{tikzpicture}}
\newcommand{\riskcosts}[1]{\begin{tikzpicture}[x=1in,y=1in]\node [centered, inner sep=0pt] at (0,0) {\includegraphics[width=0.5in,height=0.5in]{icons/risk.png}}; \node [centered, inner sep=0pt] at (0,0) {\textcolor{white}{#1}}; \end{tikzpicture}}

\newcommand{\raw}[1]{%
\node [rectangle, minimum width=650.0/300.0, minimum height=950.0/300.0, text justified, text width=53mm, inner sep=1mm, anchor=north west] at (50.0/300.0,1000.0/300.0) {#1};
}

\newcommand{\rawcenter}[1]{%
\node [rectangle, minimum width=650.0/300.0, minimum height=950.0/300.0, text centered, text width=53mm, inner sep=1mm, anchor=north west] at (50.0/300.0,1000.0/300.0) {#1};
}

\newcommand{\rawleft}[1]{%
\node [rectangle, minimum width=650.0/300.0, minimum height=950.0/300.0, text ragged, text width=53mm, inner sep=1mm, anchor=north west] at (50.0/300.0,1000.0/300.0) {#1};
}

\newcommand{\hraw}[1]{%
\node [rectangle, rotate=90, minimum width=950.0/300.0, minimum height=650.0/300.0, text justified, text width=80mm, inner sep=1mm, anchor=north west] at (50.0/300.0,50.0/300.0) {#1};
}

\newcommand{\hrawcenter}[1]{%
\node [rectangle, rotate=90, minimum width=950.0/300.0, minimum height=650.0/300.0, text centered, text width=80mm, inner sep=1mm, anchor=north west] at (50.0/300.0,50.0/300.0) {#1};
}

\newcommand{\hrawleft}[1]{%
\node [rectangle, rotate=90, minimum width=950.0/300.0, minimum height=650.0/300.0, text ragged, text width=80mm, inner sep=1mm, anchor=north west] at (50.0/300.0,50.0/300.0) {#1};
}

\newcommand{\difficulty}[2]{%
    \newcommand{\easybackgroundcolor}{#1}
    \newcommand{\hardbackgroundcolor}{#2}
}

\newcommand{\easyname}[1]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (200.0/300.0,450.0/300.0) {\bf\fontsize{16}{18}\bebas#1\par};
}

\newcommand{\hardname}[1]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (550.0/300.0,450.0/300.0) {\bf\fontsize{16}{18}\bebas#1\par};
}

\newcommand{\easyeffect}[1]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (200.0/300.0,175.0/300.0) {\bf\fontsize{10}{10}\bebas#1\par};
}

\newcommand{\hardeffect}[1]{%
    \node [rectangle, anchor=north, minimum width=1in, minimum height=1.0/3.0in, text centered, text width=1in, inner sep=0mm] at (550.0/300.0,175.0/300.0) {\bf\fontsize{10}{10}\bebas#1\par};
}

% Aspect ratio of about 3:2
\newcommand{\easyart}[1]{%
    \node [rectangle, minimum width=0.9in, minimum height=200.0/300.0, inner sep=0, anchor=north west] at (65.0/300.0,390.0/300.0) {\noindent\includegraphics*[width=0.9in, height=0.66666in]{#1}};
}

\newcommand{\hardart}[1]{%
    \node [rectangle, minimum width=0.9in, minimum height=200.0/300.0, inner sep=0, anchor=north west] at (415.0/300.0,390.0/300.0) {\noindent\includegraphics*[width=0.9in, height=0.66666in]{#1}};
}

\newcommand{\easyhypo}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,375.0/300.0) rectangle (65.0/300.0,325.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 1<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,315.0/300.0) rectangle (65.0/300.0,265.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 2<#1
        %\fill [left color=energy, right color=personal] (50.0/300.0,255.0/300.0) rectangle (65.0/300.0,205.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (50.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
}

\newcommand{\easystarve}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [right color=health, left color=food] (336.0/300.0,375.0/300.0) rectangle (350.0/300.0,325.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 1<#1
        %\fill [right color=health, left color=food] (336.0/300.0,315.0/300.0) rectangle (350.0/300.0,265.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 2<#1
        %\fill [right color=health, left color=food] (336.0/300.0,255.0/300.0) rectangle (350.0/300.0,205.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (350.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
}

\newcommand{\hardhypo}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,375.0/300.0) rectangle (415.0/300.0,325.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 1<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,315.0/300.0) rectangle (415.0/300.0,265.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
    \ifnum 2<#1
        %\fill [left color=energy, right color=personal] (400.0/300.0,255.0/300.0) rectangle (415.0/300.0,205.0/300.0);
        \node [rectangle, anchor=west, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (400.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/hypo.png}};
    \fi
}

\newcommand{\hardstarve}[1]{%
    % TODO: Improve alignment
    \ifnum 0<#1
        %\fill [right color=health, left color=food] (686.0/300.0,375.0/300.0) rectangle (700.0/300.0,325.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,360.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 1<#1
        %\fill [right color=health, left color=food] (686.0/300.0,315.0/300.0) rectangle (700.0/300.0,265.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,290.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
    \ifnum 2<#1
        %\fill [right color=health, left color=food] (686.0/300.0,255.0/300.0) rectangle (700.0/300.0,205.0/300.0);
        \node [rectangle, anchor=east, minimum width=0.25in, minimum height=0.25in, text centered, text width=0.25in, inner sep=0mm] at (700.0/300.0,220.0/300.0) {\includegraphics*[width=0.25in, height=0.25in]{icons/starve.png}};
    \fi
}

\newcommand{\easyflava}[1]{%
    \node [rectangle, minimum width=1in, minimum height=50.0/300.0, text centered, text width=1in, inner sep=0mm, anchor=north] at (200.0/300.0,75.0/300.0) {\fontsize{4}{6}\alegreya"#1"\par};
}

\newcommand{\hardflava}[1]{%
    \node [rectangle, minimum width=1in, minimum height=50.0/300.0, text centered, text width=1in, inner sep=0mm, anchor=north] at (550.0/300.0,75.0/300.0) {\fontsize{4}{6}\alegreya"#1"\par};
}

\newenvironment{card}
{% Begin
\noindent\begin{tikzpicture}[x=1in,y=1in]
\fill [\edgecolor] (0,0) rectangle (2.5,3.5);
\fill[rounded corners=10pt] [\backgroundcolor] (50.0/300.0,50.0/300.0) rectangle (700.0/300.0,1000.0/300.0);
\node at (645.0/300.0,95.0/300.0) {\includegraphics[width=0.25in, height=0.25in]{deck/\whichdeck.png}};
}
{% End
\end{tikzpicture}}

\newenvironment{playcard}[3][@]
{% Begin
\cardtype{#2}\rarity{#3}
\begin{card}
\type{\if #1@#2\else#1 #2\fi}
}
{% End
\end{card}
}

\newenvironment{board}
{% Begin
\noindent\begin{tikzpicture}[x=1in,y=1in]
%\fill [white] (0,0) rectangle (16.25,10.25);
}
{% End
\end{tikzpicture}}

\newenvironment{cardstretch}
{% Begin
\noindent\begin{tikzpicture}[x=1in,y=1in]
\fill [left color=\hardbackgroundcolor, right color=\easybackgroundcolor] (0,0) rectangle (2.5,1.75);
%\fill [\hardbackgroundcolor] (0,0) rectangle (1.25,1.75);
%\fill [\easybackgroundcolor] (1.25,0) rectangle (2.5,1.75);
\fill[rounded corners=10pt] [\easybackgroundcolor] (50.0/300.0,50.0/300.0) rectangle (350.0/300.0,475.0/300.0);
\fill[rounded corners=10pt] [\hardbackgroundcolor] (400.0/300.0,50.0/300.0) rectangle (700.0/300.0,475.0/300.0);
}
{% End
\end{tikzpicture}}

