\documentclass{article}

\usepackage[top=0.125in, bottom=0.125in, left=0.125in, right=0.125in, margin=0.125in, papersize={3.5in, 5in}, includefoot]{geometry}
\usepackage{xcolor}
\usepackage{parskip}
\usepackage{pdfpages}
\usepackage{tikz}
\usepackage{titlesec}
\usepackage{fancybox}
\usepackage{wrapfig}
\usepackage{subfig}

\titleformat{name=\section}[display]{}{\thetitle.}{0.8em}{\Heading\Large}
\titlespacing{\section}{0pt}{0pt plus 4pt minus 4pt}{0pt plus 4pt minus 4pt}
\titleformat{name=\subsection}[display]{}{\thetitle.}{0.5em}{\Heading\large}
\titlespacing{\subsection}{0pt}{0pt plus 4pt minus 4pt}{0pt plus 4pt minus 4pt}
\titleformat{name=\subsubsection}[runin]{}{\thetitle.}{0.5em}{\itshape}[.]
\titlespacing{\subsubsection}{0pt}{0pt}{0.5em}
\titleformat{name=\paragraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\paragraph}{0em}{0em}{0.5em}
\titleformat{name=\subparagraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\subparagraph}{0em}{0em}{0.5em}

\definecolor{utility}{HTML}{FFFFFF}
\definecolor{attachment}{HTML}{5FAFCF}
\definecolor{dog}{HTML}{BF9F7F}
\definecolor{leg}{HTML}{9F9F9F}
\definecolor{personal}{HTML}{AF5FCF}
\definecolor{movement}{HTML}{7F7FDF}
\definecolor{damage}{HTML}{FF6F6F}
\definecolor{sled}{HTML}{7FDF7F}
\definecolor{food}{HTML}{CFCF4F}

\usepackage{graphicx}
\usepackage{enumitem}

\usepackage{fontspec}
\setmainfont[Scale=0.8]{Merriweather}
\newfontfamily\Heading{Lato}

\setcounter{secnumdepth}{0}

\pagestyle{plain}
\pagenumbering{arabic}

\begin{document}
\begin{titlepage}
\
\vfill
\centering
\includegraphics{iditacards}\par\vspace{5em}
{\Heading\LARGE Iditacards Instruction Manual\par}
\vfill
\end{titlepage}

\tableofcontents
\clearpage

\section{Overview}
% Your background story/recap on what situation the players are getting themselves into. It sets the scene (thematically, usually) for the entire game.

Iditacards is a game where you race your opponents in the last great race on
earth - The Iditarod. You will face starvation, hypothermia, and inclement
weather. You will have to keep your sled in good condition and your dogs in top
form to come out ahead.

The first player to cross the finish line wins!

\section{Components}
% This isn’t so important for playtesters at this point, but is important for the final rulebook and print and plays (PnP). This way players (including yourself) know whether or not there are missing pieces, or in the case of PnP players, if they have everything they need in order to play.

This game contains many cards. They can be sorted based on the symbol in the
bottom right corner:

\begin{itemize}
    \setlength\itemsep{-1em}
    \item 4 Starter Decks - \input "|cat cards/decks/starter1.txt | wc -l" cards each (\includegraphics[width=1em]{images/deck/starter1.png} \includegraphics[width=1em]{images/deck/starter2.png} \includegraphics[width=1em]{images/deck/starter3.png} \includegraphics[width=1em]{images/deck/starter4.png})
    \item 1 Legendary Deck - \input "|cat cards/decks/legendary.txt | wc -l" cards (\includegraphics[width=1em]{images/deck/legendary.png})
    \item 4 Upgrade Decks - \input "|cat cards/decks/speed.txt | wc -l" cards each (\includegraphics[width=1em]{images/deck/speed.png} \includegraphics[width=1em]{images/deck/attack.png} \includegraphics[width=1em]{images/deck/upgrade.png} \includegraphics[width=1em]{images/deck/survivor.png})
    \item 1 Damage Deck - \input "|cat cards/decks/damage.txt | wc -l" cards (\includegraphics[width=1em]{images/deck/token.png})
\end{itemize}

Additionally there are 32 smaller leg cards.

The game also contains:

\begin{itemize}
    \setlength\itemsep{-1em}
    \item This manual
    \item The box the game came with
    \item One weather die with special symbols on it
    \item Two board halves
    \item 20 player tokens (5 of each colour)
\end{itemize}

\clearpage

\section{Objective}
% What the players are trying to accomplish. It should also make it clear how players are competing (free-for-all, teams, cooperative, etc.). This is the more technical/mechanical explanation of the 'Overview'. For example: “To be the last player with multiple spaceships orbiting the black hole”.

Every turn each player will move forward one space. The cards they play will
increase this, while the places they encounter will work to decrease it. When
the first player reaches the finish line the game ends and they win. The finish
line is the square \emph{after} the last square on the board.

\section{Setup}
% How to get the game ready for play. There shouldn’t be anything in here that mentions what components are used for or why they are important--save that for the 'Gameplay' section. Just make sure that in this section everything is laid out clearly--if diagrams are necessary (they almost always are) don’t be afraid to put those in!

The board comes in two halves that should just be placed together to create
a continuous playing surface.

To start the game each player must pick a colour. They take the tokens of that
colour placing them on the board at:

\begin{itemize}
    \setlength\itemsep{-1em}
    \item The starting space (on top of the text ``Start'')
    \item Both 0 spaces
    \item The ``Full'' and ``Warm'' spaces
\end{itemize}

Take the starter deck of your colour, find the \emph{Wheel Dog}, and place it
on the table in front of you. Then shuffle it and place it face down in front
of you.

Shuffle the legendary deck and deal 5 cards from it to each player. Each player
looks at their legendary cards and leaves them face-down in front of them.

Shuffle each of the leg decks and draw one leg of each type and place it on the
corresponding space on the board.

Before starting the game everybody draws 6 cards. The first player to grab the
weather die goes first. They roll it, then take their turn.

\section{Gameplay}
% The main gameplay section tells you how the game is broken up (rounds, turns, phases, etc.) and summarizes what players do in each of those stages. This section should explain the flow of the game from start to completion.

The game is broken into turns. Each turn you may either play a card, or take
a new day. At the end of your turn you move 1 myriameter (1 square). After you
finish moving the player to your left begins their turn.

During gameplay you will often discard cards. It is important to note that
discarded cards will be returned to your deck frequently. This is in contrast
to destroyed cards which are removed from the game permanently.

Many cards effect the size of your initial hand as well. There is no hard upper
limit on hand size, however if there aren't enough cards in your deck to fill
your hand, then your hand will consist only of however many cards are available
to be drawn.

% Once that’s done, you go into the gameplay specifics, which should be explained in the order in which they occur in game. This is where you explain exactly what happens during each turn, action, round, etc. You also should have sections dedicated to complicated subjects and their edge cases (for instance, our section on Collisions for “Pulled into Darkness”).
\subsection{Movement}

As in any race, movement is important. Whenever you move you add your current
speed to the distance. Many cards in the game will modify your speed.

When you move onto or past certain spaces different things happen. This is
called 'passing' the space.

If you pass another player they move back one space and you take one damage
(damage will be explained later).

The first time you pass each checkpoint
(\includegraphics[width=2em]{images/rules/checkpoint.png}) add one of your
legendary cards of your choice to your discard pile.

If you pass the checkerboard finish line you win the game.

\subsection{Card Anatomy}

\begin{tikzpicture}[x=\textwidth,y=\textwidth]
    \node at (0.5,0.5) {\includegraphics[width=10em]{cards/dogs/husky0.pdf}};

    \draw (0.5,0.8) node[anchor=south, inner sep=0.5ex] {Card Name} -- (0.5,0.71);
    \draw (0.25,0.75) node[anchor=east, inner sep=0.5ex] {Energy Cost} -- (0.35,0.65);
    \draw (0.25,0.6) node[anchor=east, inner sep=0.5ex] {Health Cost} -- (0.35,0.58);
    \draw (0.25,0.45) node[anchor=east, inner sep=0.5ex] {Risk Cost} -- (0.35,0.5);
    \draw (0.75,0.45) node[anchor=west, inner sep=0.5ex] {Card Effect} -- (0.58,0.45);
    \draw (0.75,0.2) node[anchor=west, inner sep=0.5ex] {Card Deck} -- (0.63,0.3);
    \draw (0.5,0.2) node[anchor=north, inner sep=0.5ex] {Card Type} -- (0.5,0.288);
\end{tikzpicture}

At the top of a card is its name. Along the left side of a card the costs are
listed. Behind the costs is the artwork of a card. Underneath the artwork is
the effect of a card. In the bottom-right corner of a card is a symbol
indicating which deck a card belongs to. Along the bottom the type of a card
is listed. This type is reinforced by the background colour of a card. Above the
type is the flavour text, which has no impact on the gameplay.

\clearpage

\subsection{Costs}

When a card is played its costs are paid in order from top to bottom.

\subsubsection{Health}

The first cost to be paid is the ``energy'' cost. Energy cost is paid by
choosing cards from the hand to discard. Note that discarded cards will be
returned later in the form of \emph{New Days}. For example if the energy cost of
a card is 2, then you must select 2 cards to discard from your hand to pay for
it.

\subsubsection{Energy}

The second cost to be paid is the ``health'' cost. Health cost is paid by
discarding cards from the top of the deck. For example if the health cost of
a card is 3, then you must select 3 cards to discard from your hand to pay for
it.

\subsubsection{Risk}

The final cost to be paid is the ``risk'' cost. Risk cost is paid by playing
cards from the top of the deck. These cards are played on subsequent turns. If
the player is unable to pay for these cards, then the payment fails. The card's
effect only occurs on the turn when the final risk is played, after resolving
that cards effect. If you fail to pay for the risk card it has no effect and
your turn is over.

Additional risk costs that occur during the payment of a risk will stack.

\clearpage

\subsubsection{Example}

To clarify how costs work, lets look at an example:

You want to play the husky card while you have armour and breakfast in your
hand and 21 cards left in your deck.

\begin{tabular}{c c c c}
    \includegraphics[scale=0.2]{cards/dogs/husky0.pdf} &
    \ovalbox{\includegraphics[width=0.3in]{cardback.png}} $\times 21$ &
    &
    \begin{tikzpicture}[x=1em,y=0.8em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/attachments/armour0.pdf}};
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
    \end{tikzpicture} \\
    In Play & Deck & Discard & Hand \\
\end{tabular}

To pay the energy cost you discard both cards in your hand, while to pay for the
health cost you discard the top two cards of your deck.

\begin{tabular}{c c c c}
    \includegraphics[scale=0.2]{cards/dogs/husky0.pdf} &
    \ovalbox{\includegraphics[width=0.3in]{cardback.png}} $\times 19$ &
    \begin{tikzpicture}[x=1em,y=0.6em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/attachments/armour0.pdf}};
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
        \node at (0,-2) {\includegraphics[scale=0.2]{cards/food/lunch0.pdf}};
        \node at (0,-3) {\includegraphics[scale=0.2]{cards/sleds/repair_sled0.pdf}};
    \end{tikzpicture} & \\
    In Play & Deck & Discard & Hand \\
\end{tabular}

Now you wait for your turn, then reveal the top card of your deck as the first
risk payment. You reveal breakfast, so you pay the health cost by discarding the
top three cards of your deck (upgrade sled, move, and dinner).  Once you pay for
breakfast you get its effect, so you draw three cards, filling your hand with
first aid, move, and good dog.

\begin{tabular}{c c c c}
    \begin{tikzpicture}[x=1em,y=0.8em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/dogs/husky0.pdf}};
        \node at (0,1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
    \end{tikzpicture} &
    \ovalbox{\includegraphics[width=0.3in]{cardback.png}} $\times 12$ &
    \begin{tikzpicture}[x=1em,y=0.6em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/attachments/armour0.pdf}};
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
        \node at (0,-2) {\includegraphics[scale=0.2]{cards/food/lunch0.pdf}};
        \node at (0,-3) {\includegraphics[scale=0.2]{cards/sleds/repair_sled0.pdf}};
        \node at (0,-4) {\includegraphics[scale=0.2]{cards/sleds/upgrade_sled0.pdf}};
        \node at (0,-5) {\includegraphics[scale=0.2]{cards/movement/move0.pdf}};
        \node at (0,-6) {\includegraphics[scale=0.2]{cards/food/dinner0.pdf}};
    \end{tikzpicture} &
    \begin{tikzpicture}[x=1em,y=0.8em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/personal/first_aid0.pdf}};
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/movement/move0.pdf}};
        \node at (0,-2) {\includegraphics[scale=0.2]{cards/dogs/good_dog0.pdf}};
    \end{tikzpicture} \\
    In Play & Deck & Discard & Hand \\
\end{tabular}

On your next turn you reveal lunch. Luckily breakfast refilled your hand so you
can successfully discard two cards to pay for it and draw three cards. Finally,
now that all of the risk has been paid, the effect of husky is active and you
get to add it to your sled team.

\begin{tabular}{c c c c}
    \begin{tikzpicture}[x=1em,y=0.8em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/dogs/husky0.pdf}};
        \node at (0,1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
        \node at (0,2) {\includegraphics[scale=0.2]{cards/food/lunch0.pdf}};
    \end{tikzpicture} &
    \ovalbox{\includegraphics[width=0.3in]{cardback.png}} $\times 9$ &
    \begin{tikzpicture}[x=1em,y=0.6em]
        \node at (0,0) {\includegraphics[scale=0.2]{cards/attachments/armour0.pdf}};
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/food/breakfast0.pdf}};
        \node at (0,-2) {\includegraphics[scale=0.2]{cards/food/lunch0.pdf}};
        \node at (0,-3) {\includegraphics[scale=0.2]{cards/sleds/repair_sled0.pdf}};
        \node at (0,-4) {\includegraphics[scale=0.2]{cards/sleds/upgrade_sled0.pdf}};
        \node at (0,-5) {\includegraphics[scale=0.2]{cards/movement/move0.pdf}};
        \node at (0,-6) {\includegraphics[scale=0.2]{cards/food/dinner0.pdf}};
        \node at (0,-7) {\includegraphics[scale=0.2]{cards/dogs/good_dog0.pdf}};
        \node at (0,-8) {\includegraphics[scale=0.2]{cards/personal/first_aid0.pdf}};
    \end{tikzpicture} &
    \begin{tikzpicture}[x=1em,y=0.8em]
        \node at (0,-1) {\includegraphics[scale=0.2]{cards/movement/move0.pdf}};
        \node at (0,-2) {\includegraphics[scale=0.2]{cards/sleds/repair_sled0.pdf}};
        \node at (0,-3) {\includegraphics[scale=0.2]{cards/personal/first_aid0.pdf}};
        \node at (0,-4) {\includegraphics[scale=0.2]{cards/movement/move0.pdf}};
    \end{tikzpicture} \\
    In Play & Deck & Discard & Hand \\
\end{tabular}

\subsection{New Days}

On any turn the current player may choose to take a new day. Any other players
may choose to join them in the new day or to sit it out.

All participating players in a new day abandon all current risks. They shuffle
all of their cards (other than in-play dogs and attachments) into their deck.
This includes cards in play, in their discard pile, and in hand.  Once their
decks are reshuffled, each player draws a new starting hand of 6 cards.

Once all players have decided whether or not to participate in the new day, the
player whose turn it is (the player that called the new day) rolls the weather
die to determine the weather for the day.

\subsection{Weather}

The weather die determines the weather effecting all players. Players who are
off the board (as in the start of the game) are not effected by weather.

The six sides of the die show the six kinds of weather that can happen. The
board shows a summary of what each weather's effect is. Here they are described
more fully.

\includegraphics[width=1.5em]{images/die/snowflake} Snow causes the supply to be
emptied. All cards in the supply are destroyed and removed from play.

\includegraphics[width=1.5em]{images/die/sun} Sun causes all players to have $1$
additional speed.

\includegraphics[width=1.5em]{images/die/wind} Wind causes all players to have
$5$ additional speed, however each player gets hypothermia.

\includegraphics[width=1.5em]{images/die/cloud} Cloudy weather cancels all speed
bonuses.

\includegraphics[width=1.5em]{images/die/storm} Stormy weather prevents
increased initial hand sizes from taking effect.

\includegraphics[width=1.5em]{images/die/rain} Rain causes all players to have
$3$ additional speed, however each player starves.

\section{Effects}

There are a number of effects referred to throughout the game. These effects are
described here.

\subsection{Speed}

Speed (\includegraphics[height=1em]{images/icons/speed}) is provided by some
cards and is added to the distance you move every time you move (including the
automatic movement at the end of your turn). For example if have $+3$ speed in
play, then you would move $4$ spaces at the end of your turn. You can keep track
of your speed over time using the meters on the side of the board.

\subsection{Legs}

Each coloured section of the board represents a leg of the race. The effects of
each leg are indicated by the leg cards drawn at the start of the game. These
effects are incurred by any players on the given leg. Each leg card has the
effects for a leg in the first half of the game, on the left, and one for a leg
in the second half of the game, on the right.

Many legs incur starvation or hypothermia. When you enter these tracks you
increase your starvation or hypothermia counters appropriately. The effects of
hypothermia and starvation are described in the following sections.

\subsection{Hypothermia}

Each point of hypothermia you have alternates between reducing your speed by
1 and increasing the
\includegraphics[width=1em,height=1em]{images/icons/hand.png} of each of your
cards by 1. These effects are tracked on the scale in the corner of the board.

Whenever you play a {\color{personal}personal} card you reduce your hypothermia by 1 as soon as
it hits the table, before resolving its cost, even if you fail to pay for it.

If you reach a hypothermia of 7 it can no longer increase, but you cannot move
until it is reduced.

\subsection{Starvation}

Each point of starvation you have alternates between reducing your speed by
1 and increasing the
\includegraphics[width=1em,height=1em]{images/icons/deck.png} of each of your
cards by 1. These effects are tracked on the scale in the corner of the board.

Whenever you play a {\color{food}food} card you reduce your starvation by 1 as
soon as it hits the table, before resolving its cost, even if you fail to pay
for it.

If you reach a starvation of 7 it can no longer increase, but you cannot move
until it is reduced.

\section{Card Types}

There are 7 unique types of playing cards in the game. This section describes
each of them in detail.

\subsection{\color{dog} Dogs}

When you play a dog it stays in front of you on the table and provides a passive
bonus. You may only have up to 6 dogs at a time. When a dog dies it is destroyed
unless otherwise stated. When you are instructed to sacrifice a dog you must
kill one of your own dogs. If you are unable to do so then the card has no
effect. You may also sacrifice any of your dogs at any time, typically to make
space for other dogs.

\subsection{\color{attachment} Attachments}

Attachments must be played on dogs. Unless otherwise stated dogs can only hold
one attachment each. If you have no dogs in play then attachments are discarded
when played, to no effect. You may discard your own attachments whenever you
wish. When the dog an attachment is on dies, that attachment is discarded.

Armour is a particular attachment that can protect a dog even when you are
instructed to sacrifice it.

\subsection{\color{sled} Sled}
\label{sec:sled}

Sled cards have the symbols
\includegraphics[height=1em]{images/icons/backpack.png} and
\includegraphics[height=1em]{images/icons/shopping-cart.png} on them. The number
after the \includegraphics[height=1em]{images/icons/backpack.png} indicates how
many cards to add to the supply from one of the upgrade decks of your choice.
All of these cards must be added from the same deck. The number after the
\includegraphics[height=1em]{images/icons/shopping-cart.png} indicates how many
cards from the supply you may add to your discard pile. Note that these cards
are discarded, and will be available when you next take a new day.

\subsection{\color{personal} Personal}
\label{sec:personal}

Personal cards have the symbols
\includegraphics[height=1em]{images/icons/eye.png} and
\includegraphics[height=1em]{images/icons/trash-can.png} on them. The number
after the \includegraphics[height=1em]{images/icons/eye.png} indicates how many
of the top cards of your deck to look at. The number after the
\includegraphics[height=1em]{images/icons/trash-can.png} indicates how many of
those you may destroy. Whatever cards you do not destroy you choose whether to
discard or to shuffle back into your deck.

Additionally, as mentioned above, personal cards reduce your
\includegraphics[height=1em]{images/icons/hypo.png}.

\subsection{\color{food} Food}

Most food cards provide draw. Additionally, as mentioned above, food cards
reduce your \includegraphics[height=1em]{images/icons/starve.png}.

\subsection{\color{movement} Movement}

Movement cards, as you may have guessed, provide movement. Movement was already
explained in the mechanics section above.

\subsection{Utility}

Utility cards are those which do not fall under the other categories.

\section{Icons}

This is a reference page for the icons used in this game:

\begin{itemize}
    \item[\raisebox{-0.3em}{\includegraphics[height=1.5em]{images/icons/dog-sled-icon.png}}]
        Movement, the number following it indicates how many squares move. Your
        speed is added to this to determine the final distance.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/speed.png}}]
        Speed, this is how much further you go every time you move.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/card-plus.png}}]
        Initial hand increase, this is how many extra cards you draw whenever
        you take a new day.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/eye.png}\includegraphics[height=1em]{images/icons/trash-can.png}}]
        Destroy. See the relevant section on page \pageref{sec:personal}.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/backpack.png}\includegraphics[height=1em]{images/icons/shopping-cart.png}}]
        Take. See the relevant section on page \pageref{sec:sled}.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/deck.png}}]
        Health cost. When it appears in an effect it acts as a modifier.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/hand.png}}]
        Energy cost. When it appears in an effect it acts as a modifier.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/risk.png}}]
        Risk cost. When it appears in an effect it acts as a modifier.
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/hypo.png}}]
        Hypothermia. When it appears in an effect it indicates that hypothermia
        should be increased (unless otherwise stated)
    \item[\raisebox{-0.1em}{\includegraphics[height=1em]{images/icons/starve.png}}]
        Starvation. When it appears in an effect it indicates that starvation
        should be increased (unless otherwise stated)
\end{itemize}

\subsection{Unique Cards} % E.G. Vigilance - immunity, meditate - effect

Certain cards have unique effects. These are described here.

\includegraphics[width=0.25\textwidth]{cards/damaged.pdf}
\includegraphics[width=0.25\textwidth]{cards/util/vigilance.pdf}
\includegraphics[width=0.25\textwidth]{cards/util/daylight_savings_time.pdf}
\includegraphics[width=0.25\textwidth]{cards/util/sleeping_bag.pdf}

Damaged: Whenever you take damage, add a damaged card to your discard pile. If
at any point you discard damage your turn immediately ends and your payment
fails.  Thus damage cannot be used to pay for energy cost, and if damage is
discarded as part of a health cost, the card it was used to help pay for fails
and has no effect.

Vigilance: Being immune means that weather has no effect on you. Other player's
cards also have no effect on you. You don't even take damage from passing
players. You do still however get pushed back when a player passes you.

Daylight Savings Time and Sleeping Bag: These cards make health or energy costs
free until you take a new day. Other cards with similar effects have a relative
change, such as a $-1$ indicating that costs are decreased by 1. These cards are
special in that they make the costs $0$ regardless of what they were before.

\clearpage

\includegraphics[width=0.25\textwidth]{cards/sleds/new_sled.pdf}
\includegraphics[width=0.25\textwidth]{cards/sleds/stop_and_shop.pdf}
\includegraphics[width=0.25\textwidth]{cards/sleds/satiated.pdf}
\includegraphics[width=0.25\textwidth]{cards/personal/ibuprofin.pdf}

New Sled: This card is best described with an example.

Say you have 4 cards in your hand and 2 hypothermia. Since you have
2 hypothermia your energy costs are increased by 1, so you have to discard
a card from your hand to pay for this even though normally it is free. Now you
have 2 cards in your hand (having discarded one to pay for New Sled and of
course new sled as the other).

You destroy the two remaining cards (putting them in the destroyed pile, not the
discard pile) and pick an upgrade deck to add cards to the supply from. You
destroyed 2 cards, so you add 2 cards to the supply. Now you get to take 2 cards
from the supply to put into your discard pile.

Stop And Shop/Satiated/Ibuprofin: These cards have preconditions.

Stop and shop only activates if you have not moved since your last new day. This
does not include the automatic movement at the end of your turn, only movement
cards.

Satiated only activates if you have played at least one each of Breakfast, Lunch
and Dinner since taking your last day.

Ibuprofin only activates if you have played any food card since taking your last
day.

In none of these cases does a new day called by another player but in which you
do not partake count.

\includegraphics[width=0.25\textwidth]{cards/util/utility_knife.pdf}
\includegraphics[width=0.25\textwidth]{cards/personal/meditate.pdf}

Utility Knife: This is a particularly unique card. Its risk is paid as any other
card's would be. However any cards you successfully pay for as part of the risk
(including any additional risks) have their effects twice.

For some cards this means nothing (for example a dog is still just played, as
normal), but for others this can have an impact. For example Breakfast would
draw 6 cards instead of 3, Mush would move 10 spaces plus speed, twice.

Meditate: This card appears at first to do nothing. However, reading through the
rules for personal cards clarifies what this card does. This card allows you to
look at your whole deck and choose any cards you wish to discard from it and
shuffle the rest back in.

\section{Game End}
% What initiates the end game (ie. once the last card is drawn, at the end of the fourth round etc.), when the game is actually over (ie. each player gets one more turn--including the player who drew the last card), how players tally their points, and restate what the victory requirement is/who wins.

When the first player crosses the finish line, each other player gets one last
turn. Once each player has had their last turn, whoever is furthest past the
finish line wins the game.

\subsection{Solitaire Mode}

The game can also be played alone by counting how many turns and days it takes
to win.

\vfill

{\small I hope you enjoy the game. - Louis A. Burke}
\end{document}
