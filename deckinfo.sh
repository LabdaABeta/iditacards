#!/bin/bash

WHICH_DECK=$1

DECKLIST=$(cat cards/decks/$WHICH_DECK.txt | sed 's/pdf/tex/g')

UTIL_COUNT=$(echo "$DECKLIST" | grep '/util/' | wc -l)
DOG_COUNT=$(echo "$DECKLIST" | grep '/dogs/' | wc -l)
MOVE_COUNT=$(echo "$DECKLIST" | grep '/movement/' | wc -l)
FOOD_COUNT=$(echo "$DECKLIST" | grep '/food/' | wc -l)
ATTACH_COUNT=$(echo "$DECKLIST" | grep '/attachments/' | wc -l)
PERSONAL_COUNT=$(echo "$DECKLIST" | grep '/personal/' | wc -l)
SLED_COUNT=$(echo "$DECKLIST" | grep '/sleds/' | wc -l)

ENERGIES=
HEALTHS=
RISKS=

for CARD in $DECKLIST; do
    if grep --quiet '\energy' $CARD; then
        ENERGIES="$ENERGIES $(grep '\energy' $CARD | tr -dc '0-9')"
    else
        ENERGIES="$ENERGIES 0"
    fi

    if grep --quiet '\health' $CARD; then
        HEALTHS="$HEALTHS $(grep '\health' $CARD | tr -dc '0-9')"
    else
        HEALTHS="$HEALTHS 0"
    fi

    if grep --quiet '\risk' $CARD; then
        RISKS="$RISKS $(grep '\risk' $CARD | tr -dc '0-9')"
    else
        RISKS="$RISKS 0"
    fi
done

TOT_ENERGY=$(echo "$ENERGIES" | awk '{s+=$1}END{print s}' RS=" ")
TOT_HEALTH=$(echo "$HEALTHS" | awk '{s+=$1}END{print s}' RS=" ")
TOT_RISK=$(echo "$RISKS" | awk '{s+=$1}END{print s}' RS=" ")

AVG_ENERGY=$(echo "$ENERGIES" | awk '{s+=$1}END{print s/NR}' RS=" ")
AVG_HEALTH=$(echo "$HEALTHS" | awk '{s+=$1}END{print s/NR}' RS=" ")
AVG_RISK=$(echo "$RISKS" | awk '{s+=$1}END{print s/NR}' RS=" ")

ENERGY_POINTS=$(echo "$ENERGIES" | tr ' ' '\n' | sed '/^\s*$/d' | sort -n | uniq -c)
HEALTH_POINTS=$(echo "$HEALTHS" | tr ' ' '\n' | sed '/^\s*$/d' | sort -n | uniq -c)
RISK_POINTS=$(echo "$RISKS" | tr ' ' '\n' | sed '/^\s*$/d' | sort -n | uniq -c)

echo "\utilcount{$UTIL_COUNT}"
echo "\dogcount{$DOG_COUNT}"
echo "\movecount{$MOVE_COUNT}"
echo "\foodcount{$FOOD_COUNT}"
echo "\attachcount{$ATTACH_COUNT}"
echo "\personalcount{$PERSONAL_COUNT}"
echo "\sledcount{$SLED_COUNT}"
echo "%\avgenergy{$AVG_ENERGY}"
echo "%\avghealth{$AVG_HEALTH}"
echo "%\avgrisk{$AVG_RISK}"
echo "\energycount{$TOT_ENERGY}"
echo "\healthcount{$TOT_HEALTH}"
echo "\riskcount{$TOT_RISK}"

: '
echo "\begin{costcounts}"

echo "\energycounts{"
echo "$ENERGY_POINTS" | awk '{print "(" $2 "," $1 ") "}'
echo "}"

echo "\healthcounts{"
echo "$HEALTH_POINTS" | awk '{print "(" $2 "," $1 ") "}'
echo "}"

echo "\riskcounts{"
echo "$RISK_POINTS" | awk '{print "(" $2 "," $1 ") "}'
echo "}"

echo "\end{costcounts}"
'
