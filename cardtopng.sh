#!/bin/bash

# Syntax: cardtopng.sh cardname.png
OUTPUT=$1
INTERMEDIATE=$(echo "$1" | sed 's/\[.*\]\..*//')-raw.png
PDFNAME=$(echo "$1" | sed 's/\[.*\]\..*//').pdf

# Determine card size
#if grep -q 750x1050 <<<$(identify $INTERMEDIATE); then
if [ "${PDFNAME/stretch}" = "$PDFNAME" ]; then
    # Poker Deck
    pdftoppm -scale-to-x 750 -scale-to-y 1050 -png $PDFNAME > $INTERMEDIATE
    convert $INTERMEDIATE -set option:distort:viewport 825x1125-37-37 -virtual-pixel Edge -distort SRT 0 +repage $OUTPUT
else
    # Mini Deck
    pdftoppm -scale-to-x 750 -scale-to-y 525 -png $PDFNAME > $INTERMEDIATE
    convert $INTERMEDIATE -set option:distort:viewport 825x600-37-37 -virtual-pixel Edge -distort SRT 0 +repage $OUTPUT
fi
